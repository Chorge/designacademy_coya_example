﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	[SerializeField] Text m_mainTextField;
	[SerializeField] Button m_Button_A;
	[SerializeField] Button m_Button_B;
	[SerializeField] Button m_Button_C;
	[SerializeField] Button m_Button_D;
	[SerializeField] Text m_ButtonText_A;
	[SerializeField] Text m_ButtonText_B;
	[SerializeField] Text m_ButtonText_C;
	[SerializeField] Text m_ButtonText_D;

	Chapter m_currentChapter;

	// Use this for initialization
	void Start () {

		Chapter startChapter = new Chapter("Welcome to this amazing cat adventure! What will our cat hero do today?");

		Chapter chapter_A = new Chapter("Our cat hero is sleeping at and is dreaming of ...");
		Chapter chapter_B = new Chapter("Our cat hero is exploring the forest, and gets bored.");
		Chapter chapter_C = new Chapter("Our cat hero meowing at its owner. The owner does not wohrship you!");
		Chapter chapter_D = new Chapter("Our cat hero and all the other cats overtake the world! What now?");

		startChapter.AddButton_A ("Go to sleep", chapter_A);
		startChapter.AddButton_B ("Explore the forest", chapter_B);
		startChapter.AddButton_C ("Meow at its human?", chapter_C);
		startChapter.AddButton_D ("World Domination!", chapter_D);

		chapter_A.AddButton_A ("Dreams of the Forest", chapter_B);
		chapter_A.AddButton_B ("Dreams of Humans", chapter_C);
		chapter_A.AddButton_C ("Dreams of World Domination", chapter_D);

		chapter_B.AddButton_A ("Retrun home", startChapter);

		chapter_C.AddButton_A ("Start World Domination Plan!", chapter_D);

		chapter_D.AddButton_A ("Go to sleep!", chapter_A);
		chapter_D.AddButton_B ("Meow at the last human alive", chapter_C);
		chapter_D.AddButton_D ("Reset Time to the start!", startChapter);

		UpdateNewChapter (startChapter);
	}

	void UpdateNewChapter(Chapter chapter)
	{
		m_currentChapter = chapter;
		m_currentChapter.UpdateWindow (m_mainTextField);
		m_currentChapter.UpdateWindowButton_A (m_ButtonText_A);
		m_currentChapter.UpdateWindowButton_B (m_ButtonText_B);
		m_currentChapter.UpdateWindowButton_C (m_ButtonText_C);
		m_currentChapter.UpdateWindowButton_D (m_ButtonText_D);
	}

	public void ButtonPressed_A () {

		Chapter chapter = m_currentChapter.ButtonPressed_A ();

		if (chapter != null) {
			UpdateNewChapter (chapter);
		}
	}

	public void ButtonPressed_B () {


		Chapter chapter = m_currentChapter.ButtonPressed_B ();

		if (chapter != null) {
			UpdateNewChapter (chapter);
		}
	}

	public void ButtonPressed_C () {
		
		Chapter chapter = m_currentChapter.ButtonPressed_C ();

		if (chapter != null) {
			UpdateNewChapter (chapter);
		}
	}

	public void ButtonPressed_D () {
		
		Chapter chapter = m_currentChapter.ButtonPressed_D ();

		if (chapter  != null) {
			UpdateNewChapter (chapter);
		}
	}
}
