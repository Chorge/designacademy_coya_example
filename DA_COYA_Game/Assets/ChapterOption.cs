﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class ChapterOption
{
	string m_buttonName;
	Chapter m_nextChapter;

	public ChapterOption (string name, Chapter chapter)
	{
		m_buttonName = name;
		m_nextChapter = chapter;
	}


	public void UpdateButton(Text buttonText)
	{
		buttonText.text = m_buttonName;
	}

	public Chapter ButtonPressed()
	{
		return m_nextChapter;
	}
}

